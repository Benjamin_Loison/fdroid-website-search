#!/usr/bin/env python3
#
# models.py - part of fdroid-website-search django application
# Copyright (C) 2017-2018 Michael Pöhn <michael.poehn@fsfe.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django import template
from fdroid_website_search import default_config

from fdroid_website_search.models import FDroidSite
from fdroid_website_search.models import AppI18n

import re
import fdroid_website_search.settings


register = template.Library()


def _norm_lang(lang):
    """normalize langugage code"""
    return lang.lower().replace('-', '_') if type(lang) == str else lang


def _map_lang(lang):
    l = _norm_lang(lang)
    if l == 'zh_cn':
        l = 'zh_hans'
    elif l == 'zh_tw':
        l = 'zh_hant'

    for allowed_lang in default_config.langs:
        if l == allowed_lang.lower():
            l = allowed_lang

    return l


def _get_app_filed_i18n(app, fieldname, requested_lang):
    """get correct translation for a AppI18n field."""

    requested_lang = _map_lang(requested_lang)
    available_translations = {x.lang.name: getattr(x, fieldname) for x in AppI18n.objects.filter(entry=app) if len(getattr(x, fieldname)) > 0}

    if requested_lang in available_translations.keys():
        return available_translations[requested_lang]

    if requested_lang.split('_')[0] in available_translations.keys():
        return available_translations[requested_lang.split('_')[0]]

    if 'en' in available_translations.keys():
        return available_translations['en']

    for lang in available_translations.keys():
        if lang.startswith('en'):
            return available_translations[lang]

    return ""


@register.simple_tag
def get_all_repos():
    return default_config.repos


@register.simple_tag
def get_allowed_langs():
    return default_config.langs


@register.simple_tag
def get_color_background():
    return default_config.color_background


@register.simple_tag
def get_color_background_header():
    return default_config.color_background_header


@register.simple_tag
def get_color_text():
    return default_config.color_text


@register.simple_tag(takes_context=True)
def get_link_back(context):
    l = default_config.repos[0]['site']
    if not l.endswith('/'):
      l += '/'
    l += context.request.GET.get('lang', 'en')
    return l


@register.simple_tag
def get_last_updated():
    site_object = FDroidSite.objects.first()
    if site_object:
        return '{:%Y-%m-%d}'.format(site_object.lastUpdated)
    return '-'


@register.simple_tag
def get_version():
    return fdroid_website_search.settings.VERSION


@register.simple_tag(takes_context=True)
def i18n_app_name(context, app):
    lang = context.get("request", {}).GET.get('lang', 'en')
    translated = _get_app_filed_i18n(app, "name", lang)
    return translated if translated else app.name


@register.simple_tag(takes_context=True)
def i18n_app_summary(context, app):
    lang = context.get("request", {}).GET.get('lang', 'en')
    translated = _get_app_filed_i18n(app, "summary", lang)
    return translated if translated else app.summary


@register.simple_tag(takes_context=True)
def i18n_app_url(context, app):
    res = app.site.siteUrl
    lang = context.get('lang')
    lang = context.get("request", {}).GET.get('lang', 'en')
    if lang in default_config.langs:
        res += '/' + lang
    res += '/packages/' + app.packageName
    return res


@register.simple_tag(takes_context=True)
def i18n_app_icon(context, app):
    lang = context.get("request", {}).GET.get('lang', 'en')
    translated = _get_app_filed_i18n(app, "icon", lang)
    baseurl = app.site.iconMirrorUrl
    if translated:
        return baseurl + '/' + app.packageName + translated
    elif app.icon:
        return baseurl + '/icons/' + app.icon
    else:
        return app.site.siteUrl + "/assets/ic_repo_app_default.png"


@register.simple_tag
def pagerange2(pagecount, pageposition):
    if pagecount == 1:
        return []
    elif pagecount == 2:
        return (1, 2)
    elif pagecount == 3:
        return (1, 2, 3)
    else:
        r = [x for x in range(max(1, pageposition-2), min(pageposition+3, pagecount))]
        if r[0] != 1:
            r.insert(0, 1)
        if r[1] != 2:
            r.insert(1, '...')
        if r[-1] != pagecount:
            r.append(pagecount)
        if r[-2] != pagecount-1:
            r.insert(len(r)-1, '...')
        return r


@register.filter
def search_index_camel_case_tokens(value):
    if type(value) == str:
        camel_splits = re.findall(r'[A-Z](?:[a-z]+|[A-Z]*(?=[A-Z]|$))', value)
        if len(camel_splits) > 1:
            # we don't need the first tocken
            # because the full word gets index too
            return ' '.join(camel_splits[1:])
    return ''


@register.filter
def search_index_colapse_dash_tokens(value):
    if type(value) == str:
        result = []
        for token in value.split():
            if '-' in token:
                result.append(token.replace('-', ''))
        return ' '.join(result)
    return ''

@register.filter
def search_index_simplify_unicode_tokens(value):
    mapping = {'⁰': '0',
               '¹': '1',
               '²': '2',
               '³': '3',
               '⁴': '4',
               '⁵': '5',
               '⁶': '6',
               '⁷': '7',
               '⁸': '8',
               '⁹': '9',
               '⁺': '+',
               '⁻': '-',
               '⁼': '=',
               '⁽': '(',
               '⁾': ')',
               'ⁿ': 'n'}
    if type(value) == str:
        result = []
        for unichar, simplechar in mapping.items():
            if unichar in value:
                result.append(re.sub(unichar, simplechar, value))
        return ' '.join(result)
    return ''
